package com.test.kotlin

fun getStringLength(obj: Any): Int? {
    if (obj is String) {
        // obj는 유동적으로 String 또는 다양하게
        return obj.length
    }
    return null
}

fun getStringLength1(obj: Any): Int? {
    if (obj !is String) return null

    // `obj` is automatically cast to `String` in this branch
    return obj.length
}

fun getStringLength2(obj: Any): Int? {
    // `obj` is automatically cast to `String` on the right-hand side of `&&`
    if (obj is String && obj.length > 0) {
        return obj.length
    }

    return null
}

fun main() {
    fun printLength(obj: Any){
        println("'$obj' string length is ${getStringLength(obj) ?: "... err, not a string"}")
    }

    printLength("Incomprehensibilites")
    printLength(1000)
    printLength(listOf(Any()))
}

//형식 뒤에 ?를 붙히면 Null 값을 넣을 수 있따.
