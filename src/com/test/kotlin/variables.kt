package com.test.kotlin

val PI = 3.14
var x = 0

fun incrementX() {
    x += 1
}

fun main(){
    //val은 상수
    val a: Int = 1
    val b = 2
    val c: Int
    c = 3
    println("a = $a, b = $b, c = $c")

    //var은 변수
    var y = 5
    y += 1
    println("y = $y")

    //top-level variables
    println("x = $x; PI = $PI")
    incrementX()
    println("incrementxX()")
    println("x = $x; PI = $PI")
}