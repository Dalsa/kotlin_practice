package com.test.kotlin

fun main() {
    println(sum(3,5))
    printSum(5,9)
}

fun sum(a: Int, b: Int) : Int {
    return a + b
}

fun printSum(a: Int, b: Int): Unit {
    println("sum of $a and $b is ${a+b}")
}
