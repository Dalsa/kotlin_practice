package com.test.kotlin

fun parseInt(str: String): Int? {
    return str.toIntOrNull()
}

fun printProduct(arg1: String, arg2: String) {
    val x = parseInt(arg1)
    val y = parseInt(arg2)

    //x * y  를 사용하면 null을 포함 할 수 있으므로 오류가 발생합니다.
    if (x != null && y != null) {
        // x 와 y는 null 검사 후 자동으로 nullable이 아닌 것으로 캐스팅 됩니다.
        println(x * y)
    }
    else {
        println(" '$arg1' 또는 '$arg2'는 숫자가 아닙니다.")
    }
}

fun main() {
    printProduct("6", "7")
    printProduct("a", "7")
    printProduct("a", "b")
}