package com.test.kotlin

fun main() {
    val items = listOf("apple", "banana", "kiwifruit")
    for (item in items){
        println(item)
    }

    val items1 = listOf("apple", "banana", "kiwifruit")
    for (index in items1.indices) {
        println("item at $index is ${items1[index]}")
    }
}