package com.test.kotlin

/*
fun maxOf(a: Int, b: Int): Int {
    if (a > b){
        return a
    }
    else {
        return b
    }
}
*/

fun maxOf(x: Int, y: Int) = if (x > y) x else y


fun main() {
    println("max of 14 and 12 is ${maxOf(14,12)}")
}